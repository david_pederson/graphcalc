# README #

### 2D Graphing Calculator ###

* Graphing Calculator
* 1.0

### Setup ###

* To run the calculator, download the repository or GraphingCalculator.jar file then run the java .jar file.
* Note: If building the project, the slicer folder must be added to the class path.

### Contact ###

* Emmanuel Pederson
* david.pederson@my.wheaton.edu