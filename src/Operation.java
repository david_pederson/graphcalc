
public class Operation implements ExprNode {

	/**
	 * The value at this node
	 */
	private String operator;
	
	/**
	 * Left and right subtrees
	 */
	private ExprNode left, right;
	
	public Operation(String operator, ExprNode left, ExprNode right) {
		this.operator = operator;
		this.left = left;
		this.right = right;
	}
	
	public double evaluate(double x) {
	    if(operator.equals("+")){
	        return left.evaluate(x)+right.evaluate(x);
	    }else if(operator.equals("-")){
	        return left.evaluate(x)-right.evaluate(x);
	    }else if(operator.equals("^")){
	        return Math.pow(left.evaluate(x), right.evaluate(x));
	    }else if(operator.equals("*")){
	        return left.evaluate(x)*right.evaluate(x);
	    }else if(operator.equals("/")){
	        return (left.evaluate(x)/right.evaluate(x));
	    }
	    return 1;
	}
}
