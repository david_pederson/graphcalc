import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * 
 */

/**
 * GraphCalc.java 
 * 
 * GUI and main method for the graphing calculator
 * 
 * @author David Emmanuel
 *
 */
@SuppressWarnings("serial")
public class GraphCalc extends JFrame{
	/**
	 * X length of of the panel
	 */
	static int xLength = 600;
	
	/**
	 * Y length of the panel
	 */
	static int yLength = 300;

	/**
	 * Panel provides area for graphing
	 */
	static GraphPanel panel;
	
	/**
	 * text box to enter a function
	 */
	static JTextField funcField;
	
	/**
	 * text boxes to enter graphing parameters
	 */
	static JTextField xmin, xmax, ymin, ymax;
	
	/**
	 * Object to find points to be plotted
	 */
	static PointLocator ptLocator;
	
	/**
	 * Button for repainting graph
	 */
	JButton graphButton;
	
	/**
	 * Constructor for graphing calculator a pane for graphing, fields
	 * for graphing parameters and a graph button
	 */
	public GraphCalc(){
		setTitle("Graphing Calculator");
		setContentPane(new JLabel(new ImageIcon("slicer/background.png")));
		
		setSize(xLength + 30, (int)(yLength * 1.5));
		setLocation(100, 100);
		setLayout(new FlowLayout());
		
		GraphPanel panel = new GraphPanel(xLength, yLength);
		add(panel);
		
		funcField = new JTextField(25);
		add(new JLabel("y="));
		add(funcField);
		
		xmin = new JTextField(5);
		add(new JLabel("xmin:"));
		add(xmin);
		
		xmax = new JTextField(5);
		add(new JLabel("xmax:"));
		add(xmax);	
		
		ymin = new JTextField(5);
		add(new JLabel("ymin:"));
		add(ymin);
		
		ymax = new JTextField(5);
		add(new JLabel("ymax:"));
		add(ymax);	
		
		/* Set initial values for fields */
		setDefaultFunc();
		setDefaultParams();
		
		GraphPainter gp = new GraphPainter(new PointLocator(this));
		panel.setPainter(gp);
				
		graphButton = new JButton("Graph");
		graphButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.repaint();
			}
		});
		add(graphButton);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	/**
	 * Set function to default value
	 */
	public void setDefaultFunc() {
		funcField.setText("((x ^ 3) + 1)");
	}
	
	/**
	 * Set all max/min fields to defaults
	 */
	public void setDefaultParams() {
		xmin.setText("-10");
		xmax.setText("10");
		ymin.setText("-10");
		ymax.setText("10");
	}
	
	/**
	 * Runs the graphing calculator
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new GraphCalc();		
	}
}
