
/**
 * Class to represent literal number expression nodes in
 * an expression tree.
 * 
 * @author David Emmanuel
 *
 */
public class Number implements ExprNode {

	/**
	 * The value of the number stored as a double
	 */
	double value;
	
	public Number (double value) {
		this.value = value;
	}
	
	@Override
	public double evaluate(double x) {
		return value;
	}

}
