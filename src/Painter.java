import java.awt.Graphics;

public interface Painter {

    /**
     * Update the display using the given graphics
     * object.
     * @param g The graphics object to manipulate
     */
    public void paint(Graphics g);

}
