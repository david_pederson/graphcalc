import java.io.IOException;
import java.util.ArrayList;

/**
 * PointLocator.java
 * 
 * Handles conversions of points from graph units to pixels and
 * is the parent of the process of finding all points that are
 * graphed.
 * 
 * @author David Emmanuel
 *
 */
public class PointLocator {
	/**
	 * Graphing Calculator we are working with
	 */
	private GraphCalc gc;
	
	/**
	 * Conversion factor from graph units to pixels (pixels/graphUnit)
	 */
	private double xScale, yScale;
	
	/**
	 * Graph parameters in graph units
	 */
	private double xmin, xmax, ymin, ymax;
	
	/**
	 * Constructor
	 * 
	 * @param painter object to draw points on panel
	 * @param gc graphing calculator that is running
	 */
	public PointLocator(GraphCalc gc) {
		this.gc = gc;
	}
	
	/**
	 * Find the y for each x within the visible range of the function
	 */
	public ArrayList<Coord> findPoints() {
		ArrayList<Coord> toReturn = new ArrayList<Coord>();
		
		double step = (xmax - xmin)/GraphCalc.xLength;
		for (double x = xmin; x < xmax; x += step) {
			double y;
			
			/* parse the function and evaluate */
			try {
				y = Interpreter.parse(GraphCalc.funcField.getText()).evaluate(x);
			}
			catch (Exception ao) {
				System.out.println("Enter a fully parenthasized equation in terms of x ie: ((0 - (3 * (x ^2))) + 5)");
				GraphCalc.funcField.setText("((0 - (3 * (x ^ 2))) + 5)");
				y = Interpreter.parse(GraphCalc.funcField.getText()).evaluate(x);
			}
			
			int xCoord = (int)((-xmin + x) * xScale);
			int yCoord = (int)((ymax - y) * yScale);
			
			toReturn.add((new Coord(xCoord, yCoord)));
		}
		return toReturn;
	}
	
	/**
	 * Returns the points required for the X
	 */
	public Axis<Coord, Coord> findXAxis() {
		return new Axis<Coord,Coord>(new Coord(0, (int)(GraphCalc.yLength - (-ymin) * yScale)), new Coord((int)((xmax - xmin) * xScale), (int)(GraphCalc.yLength-(-ymin) * yScale)));
	}
	
	/**
	 * Returns the points required for the Y axis
	 */
	public Axis<Coord,Coord> findYAxis() {
		return new Axis<Coord,Coord>(new Coord((int)((-xmin) * xScale), 0), new Coord((int)((-xmin) *xScale), (int)((ymax -ymin) *yScale)));
	}
	
	/**
	 * Finds all of the tic locations for each axis
	 * 
	 * @return array of coordinates with all tic positions
	 */
	public ArrayList<Coord> findTics(){
		ArrayList<Coord> allTics= new ArrayList<Coord>();
		allTics.addAll(findXTics());
		allTics.addAll(findYTics());
		return allTics;
	}
	
	private ArrayList<Coord> findYTics(){
		ArrayList<Coord> yTics= new ArrayList<Coord>();
		for(double i = ymin; i < ymax; i += 1 ) {
			yTics.add(new Coord((int)((-xmin) * xScale), (int)((-ymin + i) * yScale)));
		}
		return yTics;
	}
	
	private ArrayList<Coord> findXTics(){
		ArrayList<Coord> xTics= new ArrayList<Coord>();
		for(double i = xmin; i < xmax; i += 1 ) {
			xTics.add(new Coord((int)((-xmin +i) * xScale), (int)(GraphCalc.yLength - ((-ymin) *yScale))));
		}
		return xTics;
	}
	
	/**
	 * Get the values of the max/min text fields as doubles
	 */
	public void parseParamFields() {
		try{
			xmax = Double.parseDouble(GraphCalc.xmax.getText());
		    xmin = Double.parseDouble(GraphCalc.xmin.getText());
		    ymax = Double.parseDouble(GraphCalc.ymax.getText());
		    ymin = Double.parseDouble(GraphCalc.ymin.getText());
		    }
	    catch(Exception ioe) {
	        System.out.println("Only digits allowed in max and min fields");
	        
	        /* reset fields to defaults and try again */
	        gc.setDefaultParams();		
	        parseParamFields();		
	    }
    
		/* Check assumptions */
		if(xmax < xmin || ymax < ymin) {
			System.out.println("xmax/ymax must be greater than xmin/ymin");
	        gc.setDefaultParams();		
	        parseParamFields();	
		}
		
		/* get conversion from graph units to pixels */
		xScale = GraphCalc.xLength/(xmax - xmin);
		yScale = GraphCalc.yLength/(ymax - ymin);
	}
}
