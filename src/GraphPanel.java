import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * PaintPanel.java
 * 
 * @author David Emmanuel
 *
 */

@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	
	/**
	 * Object for adding graphics to panel
	 */
	private Painter painter;
	
	/**
	 * Pixel width and height of panel
	 */
	private int width, height;
	
	/**
	 * Constructor 
	 * @param width x length of panel in pixels
	 * @param height y length of panel in pixels
	 */
	public GraphPanel(int width, int height) {
		super();
		this.width = width;
		this.height = height;
		setSize(width, height);
		setPreferredSize(new Dimension(width, height));
		setBorder(BorderFactory.createLoweredBevelBorder());
	}
	
	/**
	 * Getter method for panel width
	 * @return panel width in pixels
	 */
	public int width() {return width; }
	
	/**
	 * Getter for panel height
	 * @return panel height in pixels
	 */
	public int height() {return height;}
	
	/**
	 * Set the the painter for this panel
	 * @param painter the Painter for the panel
	 */
	public void setPainter(Painter painter) {
		this.painter = painter;
	}
	
	/**
	 * Add graph to panel
	 * @param g
	 */
	public void paint(Graphics g) {
		super.paint(g);
		painter.paint(g);
	}
}
