/**
 * An interface for evalating nodes in an expression tree
 * 
 * @author David Emmanuel
 *
 */

public interface ExprNode {

	double evaluate(double x);
}
