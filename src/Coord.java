	/**
	 * Class to organize points to graph
	 * 
	 * @author David Emmanuel
	 *
	 */
	public class Coord {
		
		/**
		 * elements of a coordinate
		 */
		public int x, y;
		
		/**
		 * Constructor for a 2D coordinate
		 * @param x
		 * @param y
		 */
		Coord(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public String toString() {
			return "(" + x + "," + y + ")";
		}
	}
