import java.awt.*;
import java.util.ArrayList;



public class GraphPainter implements Painter {
	
	/**
	 * Hold the coordinates to be plotted
	 */
	private ArrayList<Coord> coords;
	
	/**
	 * Hold the tic coordinates to be plotted
	 */
	private ArrayList<Coord> tics;
	
	/**
	 * Axis are made up of two points
	 */
	private Axis<Coord, Coord> xAxis, yAxis;
	
	/**
	 * Class to find all points that need to be graphed
	 */
	private PointLocator ptLocator;
	
	public GraphPainter(PointLocator ptLocator) {
		this.ptLocator = ptLocator;
		findAll();
	}
	
	/**
	 * Find all of the function and axis points
	 */
	private void findAll() {
		ptLocator.parseParamFields();
		this.coords = ptLocator.findPoints();
		this.xAxis = ptLocator.findXAxis();
		this.yAxis = ptLocator.findYAxis();
		this.tics = ptLocator.findTics();
	}
	
	/**
	 * Adds all graph components to graph
	 */
	public void paint(Graphics g) {
		/* Populate points and axis fields */
		findAll();
		
		/* draw axis */
		drawAxis(g, xAxis);
		drawAxis(g, yAxis);
		
		/* draw tics along each axis */
		drawAxisTics(g);
		
		/* draw points on graph (point locator populates coords array) */
		drawFunc(g);
	}
	
	/**
	 * Draw a line from one coordinate to the other
	 * @param g
	 */
	private void drawAxis(Graphics g, Axis<Coord, Coord> axis) {
		g.drawLine(axis.pt1.x, axis.pt1.y, axis.pt2.x, axis.pt2.y);
	}
	
	private void drawAxisTics(Graphics g) {
		for(int i = 0; i < tics.size(); i ++) {
			g.drawRect(tics.get(i).x, tics.get(i).y, 2, 2);
		}
	}
	
	/**
	 * Iterate through the coordinate list to draw line for function
	 * @param g 
	 */
	private void drawFunc(Graphics g) {
		for(int i = 0; i < coords.size() - 1; i ++) {
			g.drawLine(coords.get(i).x, coords.get(i).y, coords.get(i +1).x, coords.get(i + 1).y);
		}
	}
}

