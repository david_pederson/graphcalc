/**
 * Helper class to represent the two points that make an axis
 * basically just a generic tuple.
 * 
 * @author David Emmanuel
 *
 * @param <X> 
 * @param <Y>
 */
public class Axis<A, B> {
	public final A pt1;
	public final B pt2;
	public Axis(A pt1, B pt2) {
		this.pt1 = pt1;
		this.pt2 = pt2;
	}
	
	public String toString() {
		return "(" + pt1.toString() + "," + pt2.toString() + ")";
	}
}
