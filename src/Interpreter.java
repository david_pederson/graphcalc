/**
 * Interpreter.java
 * 
 * Class to generate an ExprNode tree based on a given string
 * expression
 * 
 * @author David Emmanuel
 *
 */
public class Interpreter {
	
	/**
	 * The variable in the expression
	 */
	private static double x;
	
	/**
	 * Turn a string into an ExprNode tree
	 * 
	 * @param input The expression as a string
	 * @return The root of the expression tree
	 */
	public static ExprNode interpret(String input) {
		return parse(input);
	}
	
	/**
	 * Testing for expression interpreter
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		x = Double.parseDouble(args[1]);
		ExprNode tree = parse(args[0]);
		System.out.println(tree.evaluate(x));
	}
	
	/**
	 * Recursive function to make an expression in string format into an ExprTree
	 * 
	 * @param expr String to be parsed 
	 * @return ExprNode root of the expression tree
	 */
	public static ExprNode parse(String expr) {
		String[] arr = ExprStringSlicer.slice(expr);
		if(arr.length == 1) {
			if (arr[0].equals("x")){
				ExprNode op = new Variable();
				return op;
			}
			else {
				ExprNode op = new Number(Double.parseDouble(arr[0]));
				return op;
			}
		}
		else {
			ExprNode op = new Operation(arr[1], parse(arr[0]), parse(arr[2]));
			return op;
		}
	}
}
